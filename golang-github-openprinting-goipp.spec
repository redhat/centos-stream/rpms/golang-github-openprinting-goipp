# Generated by go2rpm 1
%bcond_without check

# https://github.com/OpenPrinting/goipp
%global goipath         github.com/OpenPrinting/goipp
Version:                1.1.0

%gometa

%global common_description %{expand:
Package goipp implements the IPP core protocol in pure Go (RFC 8010).}

%global golicenses      LICENSE
%global godocs          README.md index.md

Name:           %{goname}
Release:        6%{?dist}
Summary:        IPP core protocol in pure Go (RFC 8010)

# Upstream license specification: BSD-2-Clause
License:        BSD-2-Clause
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep
%autopatch

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 1.1.0-6
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Jul 22 2024 Zdenek Dohnal <zdohnal@redhat.com> - 1.1.0-5
- RHEL-49443 Revert change in goipp regarding IPP standard breaking change

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 1.1.0-4
- Bump release for June 2024 mass rebuild

* Wed Jan 24 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sat Jan 20 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Thu Jan 11 2024 Zdenek Dohnal <zdohnal@redhat.com> - 1.1.0-1
- 2253306 - golang-github-openprinting-goipp-1.1.0 is available

* Thu Nov 23 2023 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-9
- Rebuild for any CVE fixes in golang

* Wed Jul 26 2023 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-8
- SPDX migration

* Thu Jul 20 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Aug 25 16:16:26 CEST 2020 Zdenek Dohnal <zdohnal@redhat.com> - 1.0.0-1
- Initial import (bz#1874149)

